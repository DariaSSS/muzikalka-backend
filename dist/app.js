"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require('express');
const bodyParser = require("body-parser");
const Router_1 = require("./Router");
const passport = require('passport');
require("./dbconnection");
require("./passport");
const app = express();
app.use(passport.initialize());
app.use(bodyParser.json());
app.use('/api', Router_1.router);
app.use("/api/files", express.static(__dirname + '/../uploads'));
app.listen(3500, () => {
    console.log('сервер запущен');
});
//# sourceMappingURL=app.js.map