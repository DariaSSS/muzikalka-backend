"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getArtistAndSongname = void 0;
const MusicMetadata = require('music-metadata');
const fs = require('fs');
const getArtistAndSongname = (filename) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    const metadata = yield MusicMetadata.parseFile(`uploads/${filename}`);
    let artist = metadata.common.artist
        ? metadata.common.artist
        : 'Неизвестен';
    let songname = metadata.common.title
        ? metadata.common.title
        : 'Без названия';
    let duration = metadata.format.duration
        ? metadata.format.duration
        : 0;
    let picture = ((_a = metadata.common) === null || _a === void 0 ? void 0 : _a.picture) && ((_b = metadata.common) === null || _b === void 0 ? void 0 : _b.picture[0])
        ? metadata.common.picture[0].data.toString('base64')
        : null;
    if (picture !== null) {
        picture = filename + '.jpg';
        fs.writeFile(`uploads/${picture}`, metadata.common.picture[0].data.toString('base64'), 'base64', function (err) {
            console.log(err);
        });
    }
    return [artist, songname, duration, picture];
});
exports.getArtistAndSongname = getArtistAndSongname;
//# sourceMappingURL=music-metadata.js.map