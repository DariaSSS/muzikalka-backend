"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JWT_SECRET_KEY = exports.passport = void 0;
const passportJWT = require('passport-jwt');
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const passport = require('passport');
exports.passport = passport;
const JWT_SECRET_KEY = 'your_jwt_secret';
exports.JWT_SECRET_KEY = JWT_SECRET_KEY;
passport.use(new JWTStrategy({ jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: JWT_SECRET_KEY }, (jwtPayload, done) => {
    const user = jwtPayload.user;
    return done(null, user);
}));
passport.serializeUser((user, done) => {
    done(null, user);
});
passport.deserializeUser((user, done) => {
    done(null, user);
});
//# sourceMappingURL=passport.js.map