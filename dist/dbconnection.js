"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getEntityManager = void 0;
const typeorm_1 = require("typeorm");
typeorm_1.createConnection({
    name: "default",
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "root",
    database: "muzikalka",
    entities: [__dirname + '/entities/*.js']
});
const getEntityManager = () => typeorm_1.getConnection().createEntityManager();
exports.getEntityManager = getEntityManager;
//# sourceMappingURL=dbconnection.js.map