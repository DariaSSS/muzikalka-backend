"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const typeorm_1 = require("typeorm");
const Song_1 = require("./Song");
let User = class User {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn()
], User.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        type: 'varchar'
    })
], User.prototype, "login", void 0);
__decorate([
    typeorm_1.Column({
        type: 'varchar',
        select: false
    })
], User.prototype, "password", void 0);
__decorate([
    typeorm_1.ManyToMany(() => Song_1.Song, (song) => song.users),
    typeorm_1.JoinTable({
        name: 'user_music',
        joinColumn: {
            name: 'song_id'
        }
    })
], User.prototype, "songs", void 0);
User = __decorate([
    typeorm_1.Entity('users')
], User);
exports.User = User;
//# sourceMappingURL=User.js.map