"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Song = void 0;
const typeorm_1 = require("typeorm");
const User_1 = require("./User");
let Song = class Song {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn()
], Song.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({
        type: "varchar"
    })
], Song.prototype, "artist", void 0);
__decorate([
    typeorm_1.Column({
        type: "varchar"
    })
], Song.prototype, "songname", void 0);
__decorate([
    typeorm_1.Column({
        type: "varchar"
    })
], Song.prototype, "fileName", void 0);
__decorate([
    typeorm_1.Column({
        type: "integer",
        nullable: true
    })
], Song.prototype, "duration", void 0);
__decorate([
    typeorm_1.Column({
        type: "varchar",
        nullable: true
    })
], Song.prototype, "picture", void 0);
__decorate([
    typeorm_1.ManyToMany(() => User_1.User, user => user.songs, {}),
    typeorm_1.JoinTable({
        name: 'user_music',
        joinColumn: {
            name: 'user_id',
            referencedColumnName: 'id'
        }
    })
], Song.prototype, "users", void 0);
Song = __decorate([
    typeorm_1.Entity('songs')
], Song);
exports.Song = Song;
//# sourceMappingURL=Song.js.map