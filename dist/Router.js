"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const Song_1 = require("entities/Song");
const User_1 = require("entities/User");
const express_1 = require("express");
const passport_1 = require("./passport");
const typeorm_1 = require("typeorm");
const dbconnection_1 = require("./dbconnection");
const multer_middleware_1 = require("./multer-middleware");
const music_metadata_1 = require("helper/music-metadata");
const passport = require('passport');
const jwt = require('jsonwebtoken');
const router = express_1.Router();
exports.router = router;
router.get('/', (req, res, next) => {
    res.send('respond with a resource');
});
router.use('/profile', passport.authenticate('jwt'));
router.get('/profile', (req, res) => {
    res.json(req.user);
});
router.post('/profile/upload', multer_middleware_1.upload.array('songs', 10), (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const files = req.files;
    if (files.length === 0) {
        res
            .status(500)
            .send("Ошибка при загрузке файла");
        return;
    }
    res.send("Файл загружен");
    const entityManager = dbconnection_1.getEntityManager();
    for (const file of files) {
        const [artist, songname, duration, picture] = yield music_metadata_1.getArtistAndSongname(file.filename);
        const songs = entityManager.create(Song_1.Song, {
            artist,
            songname,
            fileName: file.filename,
            duration,
            picture
        });
        yield entityManager.save(songs);
    }
}));
router.post('/registration', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.send('hi');
    const entityManager = dbconnection_1.getEntityManager();
    const user = entityManager.create(User_1.User, req.body);
    yield entityManager.save(user);
}));
router.post('/auth', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { login, password } = req.body;
    const user = yield typeorm_1.getConnection().getRepository(User_1.User).findOne({
        where: {
            login, password
        },
    });
    if (!user) {
        res.status(401)
            .json({
            msg: "Неправильный логин или пароль"
        });
    }
    passport.authenticate('jwt', { session: false }, () => {
        req.login(user, { session: false }, (err) => {
            if (err) {
                res.send(err);
            }
            const token = jwt.sign({ user }, passport_1.JWT_SECRET_KEY);
            return res.json({ token });
        });
    })(req, res);
}));
router.use('/songs', passport.authenticate('jwt'));
router.get('/songs', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const filter = req.query.filter;
    let qbuilder = typeorm_1.getConnection()
        .createQueryBuilder()
        .from(Song_1.Song, 's')
        .leftJoin('user_music', 'um', 's.id = um.song_id AND um.user_id = :ID', { ID: req.user.id });
    if (filter !== '') {
        qbuilder = qbuilder.andWhere('s.filename LIKE :filename', {
            filename: `%${filter}%`
        });
    }
    const songs = yield qbuilder.getRawMany();
    console.log(songs);
    res.json(songs);
}));
router.post('/songs/like', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { song_id } = req.body;
    const user_id = req.user.id;
    const existRaw = yield typeorm_1.getConnection()
        .query(`select count(user_id) as count from user_music where user_id = ? and song_id = ?`, [user_id, song_id]);
    if (existRaw[0].count > 0) {
        res.send('Дублирование запрещено');
        return;
    }
    yield typeorm_1.getConnection()
        .query(`INSERT into user_music (user_id, song_id) values (?,?)`, [user_id, song_id]);
    res.send('done');
}));
router.post('/songs/dislike', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { song_id } = req.body;
    const user_id = req.user.id;
    yield typeorm_1.getConnection()
        .query(`DELETE from user_music where user_id = ? and song_id = ?`, [user_id, song_id]);
    res.send('done');
}));
//# sourceMappingURL=Router.js.map