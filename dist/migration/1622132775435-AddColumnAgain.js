"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddColumnAgain1622132775435 = void 0;
const typeorm_1 = require("typeorm");
class AddColumnAgain1622132775435 {
    up(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.addColumn("songs", new typeorm_1.TableColumn({
                name: "duration",
                type: "integer",
                isNullable: true
            }));
            yield queryRunner.addColumn("songs", new typeorm_1.TableColumn({
                name: "picture",
                type: "varchar",
                isNullable: true
            }));
        });
    }
    down(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.dropColumn("songs", "duration");
            yield queryRunner.dropColumn("songs", "picture");
        });
    }
}
exports.AddColumnAgain1622132775435 = AddColumnAgain1622132775435;
//# sourceMappingURL=1622132775435-AddColumnAgain.js.map