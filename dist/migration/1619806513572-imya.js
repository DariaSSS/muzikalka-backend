"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.imya1619806513572 = void 0;
const typeorm_1 = require("typeorm");
class imya1619806513572 {
    up(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.createTable(new typeorm_1.Table({
                name: "users",
                columns: [
                    {
                        name: "id",
                        type: "int",
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: "login",
                        type: "varchar",
                    },
                    {
                        name: "password",
                        type: "varchar"
                    },
                ]
            }), true);
            yield queryRunner.createTable(new typeorm_1.Table({
                name: "songs",
                columns: [
                    {
                        name: "id",
                        type: "int",
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment',
                    },
                    {
                        name: "artist",
                        type: "varchar",
                    },
                    {
                        name: "songname",
                        type: "varchar"
                    }
                ]
            }), true);
            yield queryRunner.createTable(new typeorm_1.Table({
                name: "user_Music",
                columns: [
                    {
                        name: "user_id",
                        type: "int"
                    },
                    {
                        name: "song_id",
                        type: "int"
                    }
                ]
            }), true);
            yield queryRunner.createForeignKey("user_Music", new typeorm_1.TableForeignKey({
                columnNames: ["user_id"],
                referencedColumnNames: ["id"],
                name: 'user_music_user_id',
                referencedTableName: "users",
                onDelete: "CASCADE"
            }));
            yield queryRunner.createForeignKey("user_Music", new typeorm_1.TableForeignKey({
                columnNames: ["song_id"],
                referencedColumnNames: ["id"],
                referencedTableName: "songs",
                name: 'user_music_song_id',
                onDelete: "CASCADE"
            }));
        });
    }
    down(queryRunner) {
        return __awaiter(this, void 0, void 0, function* () {
            yield queryRunner.dropForeignKey('user_Music', 'user_music_user_id');
            yield queryRunner.dropForeignKey('user_Music', 'user_music_song_id');
            yield queryRunner.dropTable("users");
            yield queryRunner.dropTable("songs");
            yield queryRunner.dropTable("user_Music");
        });
    }
}
exports.imya1619806513572 = imya1619806513572;
//# sourceMappingURL=1619806513572-imya.js.map